//
//
// print.rs
// Contains all the functions that print text to stdout
//
//

use getopts::Options;

// Arguments help
pub fn print_opts_help(opts: &Options, error: Option<String>) {
    // Checks for inputted error
    if let Some(error_string) = error {
        println!("Error: {}", error_string)
    }
    let brief = format!("Usage: tlspot [options]");
    print!("{}", opts.usage(&brief));
}

// Application help
pub fn print_app_help() {
    println!("HELP -- Prints this page\nADDPERSON -- Creates a new Person in database\nSEARCH -- Fetches info about a person/people based on term\nEXIT -- Exits the interface")
}

// Application error
pub fn print_app_error(bad_command: String) {
    println!("Incorrect Command '{}'\nPass in 'HELP' for a list of commands.", bad_command)
}
