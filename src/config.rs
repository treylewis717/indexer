//
//
// config.rs
// Contains all functions pertaining to user configs (config.toml)
//
//

use std::{
    fs::{read_to_string, DirBuilder},
    path::Path,
    process::exit,
};

use serde::{Deserialize, Serialize};

// Stores config in memory
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    pub database_path: Option<String>,
}

impl Config {
    // Opens config
    pub fn open(config_path: Option<String>) -> Self {

        // Checks if a config was passed in with -c
        let path = if let Some(path) = config_path {
            path
        } else {
            format!("{}{}", get_config_dir(), "/config.toml")
        };

        let config_path = Path::new(path.as_str());

        // Errors if config does not exist
        if !config_path.exists() {
            println!(
                "Please create a config file at {} or pass in the path to your config with -c.",
                path
            );
            exit(1)
        }

        let clientconfig: Self =
            toml::from_str(read_to_string(config_path).unwrap().as_str()).unwrap();
        return clientconfig;
    }
}

// Fetches config directory for os
pub fn get_config_dir() -> String {
    match dirs::config_dir() {
        Some(config_dir) => {
            let mut pathbuf = config_dir.clone();

            pathbuf.push("indexer");

            let path = pathbuf.as_path();

            match path.exists() {
                false => {
                    DirBuilder::new().recursive(true).create(path).unwrap();
                }
                _ => {}
            }
            return String::from(path.to_str().unwrap());
        }
        None => panic!("Could not get config directory"),
    }
}
