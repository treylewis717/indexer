use std::io::Error;

use crate::{
    store::{Database, Person},
    user_input::user_input,
};

pub fn add_person(db: &mut Database) -> Result<(), Error> {
    println!();

    let name = user_input("Name: ".to_string())?;

    let access = user_input("Access: ".to_string())?;

    let role = user_input("Role: ".to_string())?;

    // Calculates the id based on the name
    let id = calc_id(name.clone(), &db);

    println!("{} {} {}", name, access, role);

    // Pushes the inputted info to the vector stored in db
    db.people.push(Person {
        name: name.to_string(),
        access: access.to_string(),
        role: role.to_string(),
        id: id.to_string().clone(),
    });

    // Writes struct to file
    db.write().expect("Could not write to file.");

    println!("Registered: {name} as ID {id}\n");

    Ok(())
}

fn calc_id(name: String, db: &Database) -> i128 {
    let mut id: i128 = 1;

    // Matches characters to a value that is then multiplied into the id #
    for c in name.to_lowercase().chars() {
        id = match c {
            'a' => id * 1,
            'b' => id * 2,
            'c' => id * 3,
            'd' => id * 4,
            'e' => id * 5,
            'f' => id * 6,
            'g' => id * 7,
            'h' => id * 8,
            'i' => id * 9,
            'j' => id * 10,
            'k' => id * 11,
            'l' => id * 12,
            'm' => id * 13,
            'n' => id * 14,
            'o' => id * 15,
            'p' => id * 16,
            'q' => id * 17,
            'r' => id * 18,
            's' => id * 19,
            't' => id * 20,
            'u' => id * 21,
            'v' => id * 22,
            'w' => id * 23,
            'x' => id * 24,
            'y' => id * 25,
            'z' => id * 26,
            _ => id * 1,
        }
    }

    // Confirms that there are at least 7 digits
    while (id.to_string().chars().collect::<Vec<char>>()).len() < 7 {
        id = id * 10;
        println!("add");
    }

    // Shortens the value to 7 digits
    while (id.to_string().chars().collect::<Vec<char>>()).len() > 7 {
        id = id / 10;
        println!("rm");
    }

    // Confirms that there are no matching values
    loop {
        let mut matched = false;

        for person in db.people.clone() {
            if id.to_string() == person.id {
                matched = true
            }
        }
        if !matched {
            break;
        }
        id += 1;
    }

    // Covers for the fact that 9999999 goes to 10000000
    if (id.to_string().chars().collect::<Vec<char>>()).len() > 7 {
        while (id.to_string().chars().collect::<Vec<char>>()).len() > 7 {
            id = id / 10;
            println!("Rm digit")
        }

        // Extra check just in case
        loop {
            let mut matched = false;

            for person in db.people.clone() {
                if id.to_string() == person.id {
                    matched = true
                }
            }
            if !matched {
                break;
            }
            id += 1;
        }
    }

    return id;
}

pub fn search(db: &Database) -> Result<(), Error> {
    println!();

    println!("Methods: NAME, ACCESS, ROLE, ID");
    println!("Type in EXIT to return to command prompt");

    println!();

    loop {
        match user_input("Method: ".to_string())?
            .to_ascii_uppercase()
            .as_str()
        {
            // Searches based on name
            "NAME" => {
                println!();
                let name = user_input("Name: ".to_string())?;

                // Checks if a person was found
                let mut printed = false;

                // Searches for person in database
                for person in &db.people {
                    // If match is found, print info
                    if person.name == name {
                        println!(
                            "\nName: {}\nAccess: {}\nRole: {}\nID: {}",
                            person.name, person.access, person.role, person.id
                        );
                        printed = true;
                    }
                }

                // Prints error if person not found
                if printed == false {
                    println!("Failed to find person/people.")
                }
                println!();
                break;
            }
            // Searches based on access level
            "ACCESS" => {
                println!();
                let access = user_input("Access: ".to_string())?;

                // Checks if a person was found
                let mut printed = false;

                // Searches for person in database
                for person in &db.people {
                    // If match is found, print info
                    if person.access == access {
                        println!(
                            "\nName: {}\nAccess: {}\nRole: {}\nID: {}",
                            person.name, person.access, person.role, person.id
                        );
                        printed = true;
                    }
                }

                // Prints error if person not found
                if printed == false {
                    println!("Failed to find person/people.")
                }
                println!();
                break;
            }
            // Searches based on role
            "ROLE" => {
                println!();
                let role = user_input("Role: ".to_string())?;

                // Checks if a person was found
                let mut printed = false;

                // Searches for person in database
                for person in &db.people {
                    // If match is found, print info
                    if person.role == role {
                        println!(
                            "\nName: {}\nAccess: {}\nRole: {}\nID: {}",
                            person.name, person.access, person.role, person.id
                        );
                        printed = true;
                    }
                }

                // Prints error if person not found
                if printed == false {
                    println!("Failed to find person/people.")
                }
                println!();
                break;
            }
            // Searches based on id
            "ID" => {
                println!();
                let id = user_input("ID: ".to_string())?;

                // Checks if a person was found
                let mut printed = false;

                // Searches for person in database
                for person in &db.people {
                    // If match is found, print info
                    if person.id == id {
                        println!(
                            "Name: {}\nAccess: {}\nRole: {}\nID: {}",
                            person.name, person.access, person.role, person.id
                        );
                        printed = true;
                        break;
                    }
                }

                // Prints error if person not found
                if printed == false {
                    println!("Failed to find person/people.")
                }
                println!();
                break;
            }
            // Exits to the app's command prompt
            "EXIT" => {
                println!();
                break;
            }
            _ => println!("Not a valid method. Please try again."),
        }
    }
    Ok(())
}
