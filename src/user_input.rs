use std::{io, io::{Error, Write}};

// Will accept user input
// Only compiles for Linux and MacOS to account for the different new-line formatting
#[cfg(any(target_os = "linux", target_os = "macos"))]
pub fn user_input(prompt: String) -> Result<String, Error> {


    let mut input = String::new();

    print!("{}", prompt);

    io::stdout().flush()?;

    io::stdin()
        .read_line(&mut input)
        .expect("Did not enter a correct string");

    if let Some('\n') = input.chars().next_back() {
        input.pop();
    }

    return Ok(input);
}

// Will accept user input
// Only compiles for Windows to account for the different new-line formatting
#[cfg(any(target_os = "windows"))]
pub fn user_input(prompt: String) -> Result<String, Error> {
    let mut input = String::new();

    print!("{}", prompt);

    io::stdout().flush()?;

    io::stdin()
        .read_line(&mut input)
        .expect("Did not enter a correct string");

    if let Some('\n') = input.chars().next_back() {
        input.pop();
    }

    if let Some('\r') = input.chars().next_back() {
        input.pop();
    }

    return Ok(input);
}
