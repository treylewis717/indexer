//
//
// store.rs
// Contains all functions pertaining to the database
//
//

use std::{
    fs::{read_to_string, write, DirBuilder, File},
    io::Error,
    path::Path,
};

use serde::{Deserialize, Serialize};

use crate::config::get_config_dir;

// Handles info to be written and read to database
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Database {
    // List of people in database; can be modified without writing to file
    pub people: Vec<Person>,

    // Path to db; can be None
    pub path: Option<String>,
}

// Stores info for struct
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Person {
    // Name of person
    pub name: String,
    // Role of person
    pub role: String,
    // Access level of person
    pub access: String,
    // ID of person
    pub id: String,
}

impl Database {
    // Opens database into memory
    pub fn open(db_path: Option<String>) -> Result<Self, Error> {
        // Checks if database's path is defined in config
        let path = if let Some(path) = db_path {
            path
        } else {
            format!("{}{}", get_config_dir(), "/database.json")
        };

        let db_path = Path::new(path.as_str());

        let parent_dir = db_path.parent().unwrap();

        // Creates parent directory if it does not exist
        if !parent_dir.exists() {
            DirBuilder::new()
                .recursive(true)
                .create(parent_dir)
                .expect(format!("Failed to create dir {}", parent_dir.to_str().unwrap()).as_str());
        }

        // Creates database if it does not exist; else, read database
        let mut db: Self = if !db_path.exists() {
            File::create(db_path)?;
            Database {
                people: Vec::new(),
                path: None,
            }
        } else {
            // Reads database
            let file_contents = read_to_string(db_path).unwrap();
            // If nothing in file, empty
            if file_contents == "" {
                Database {
                    people: Vec::new(),
                    path: None,
                }
            } else {
                serde_json::from_str(file_contents.as_str()).unwrap()
            }
        };

        db.path = Some(path);

        return Ok(db);
    }

    // Writes to file
    pub fn write(&self) -> Result<(), Error> {
        let db_path = Path::new(self.path.as_ref().unwrap().as_str());

        let string = serde_json::to_string_pretty(&self).unwrap();

        write(db_path, string).expect("Could not create cache file");
        Ok(())
    }
}
