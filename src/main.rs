//
//
// main.rs
// Main Entry File
//
//

use std::{
    env,
    io::Error,
    process::exit,
};

use command::search;
use getopts::Options;

use print::{print_app_error, print_app_help};
use store::Database;

use crate::{command::add_person, config::Config, print::print_opts_help, user_input::user_input};

mod config;
mod print;
mod store;
mod command;
mod user_input;

// Entry Function
fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();

    // Handles command arguments
    let mut opts = Options::new();
    opts.optflag("h", "help", "Prints this help menu.");
    opts.optopt(
        "c",
        "config",
        "Overrides the port set by the config",
        "PATH",
    );

    let matches = match opts.parse(&args[1..]) {
        Ok(matches) => matches,
        Err(e) => {
            print_opts_help(&opts, Some(e.to_string()));
            exit(1)
        }
    };

    if matches.opt_present("h") {
        print_opts_help(&opts, None);
        exit(0)
    }

    // Handles user config
    let config = if let Some(path) = matches.opt_str("c") {
        Config::open(Some(path))
    } else {
        Config::open(None)
    };

    // Handles database location
    let mut db = Database::open(config.database_path.clone())?;

    // Handles commands inputted through the app's CLI Interface
    loop {
        let input = user_input("Indexer: ".to_string())?;

        match input.to_ascii_uppercase().as_str() {
            // Prints Help
            "HELP" => print_app_help(),

            // Adds person to database
            "ADDPERSON" => {
                match add_person(&mut db) {
                    Ok(_) => (),
                    Err(e) => return Err(e),
                }
            }

            // Searches database based on inputted info
            "SEARCH" => {
                match search(&db) {
                    Ok(_) => (),
                    Err(e) => return Err(e),
                }
            },
            // Exits application
            "EXIT" => break,
            // No command prints no output
            "" => {}
            // Wrong command pritns error
            e => print_app_error(e.to_string()),
        }
    }

    Ok(())
}


